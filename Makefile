CXX=g++
CXXFLAGS=-std=c++17 -Wall -pedantic -g -fprofile-arcs -ftest-coverage
CC=g++
LDFLAGS = --coverage -lgcov


TARGET = src/hangman

TEST_TARGET = tests

SOURCE_PREFIX = src

TEST_PREFIX = test

BUILD_DEPS = $(SOURCE_PREFIX)/hangman.o $(SOURCE_PREFIX)/main.o

TEST_DEPS = $(SOURCE_PREFIX)/hangman.o $(TEST_PREFIX)/tests.o

.PHONY: all
all: $(TARGET)

$(TARGET) : $(BUILD_DEPS)

.PHONY: test
test: $(TEST_DEPS)
	$(CXX) $(CXX_FLAGS) $(TEST_DEPS) -o $(TEST_PREFIX)/$(TEST_TARGET) $(LDFLAGS)
	./$(TEST_PREFIX)/$(TEST_TARGET)

.PHONY: clean
clean:
	rm -f $(TARGET) *.xml *.gc?? ${SOURCE_PREFIX}/*.o ${SOURCE_PREFIX}/*.gc?? ${TEST_PREFIX}/*.o ${TEST_PREFIX}/*.gc?? ${TEST_PREFIX}/${TEST_TARGET}


.PHONY: pretty
pretty:
	clang-format $(SOURCE_PREFIX)/*.cpp $(SOURCE_PREFIX)/*.h -i -style="{BasedOnStyle: google, IndentWidth: 4}"

.PHONY: linting
linting:
	! clang-format -i src/*.cpp src/*.h -style="{BasedOnStyle: google, IndentWidth: 4}" -output-replacements-xml | grep "replacement offset"
