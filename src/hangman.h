#pragma once

#include <chrono>
#include <iostream>
#include <random>
#include <string>
#include <vector>

enum class game_status {
    playing,
    win,
    lose,
};

class Hangman {
    const std::vector<std::string> vocabulary = {
        "tree",    "leaf",      "root",     "branch",   "forest",
        "thicket", "fox",       "mushroom", "flower",   "grass",
        "bird",    "butterfly", "animal",   "squirrel", "wolf"};
    std::string hidden_word;
    std::string guessed_word;
    int max_mistakes;
    int mistakes;
    game_status status = game_status::playing;

   public:
    Hangman();

    Hangman(std::string word, int max_mistakes);

    int get_mistakes() const;

    int get_max_mistakes() const;

    std::string get_guessed_word() const;

    game_status get_status() const;

    std::string get_random_word() const;

    bool hit_or_missed(char guessed_letter);
};
