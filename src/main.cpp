#include <iostream>

#include "hangman.h"

void interface_output(std::string &message) { std::cout << message; }

char interface_input() {
    std::string message = "Guess a letter:\n";
    interface_output(message);
    char letter;
    std::cin >> letter;
    return letter;
}

int main() {
    Hangman hangman;
    while (hangman.get_status() == game_status::playing) {
        char letter = interface_input();
        if (hangman.hit_or_missed(letter)) {
            std::string hit_message = "Hit!\n";
            interface_output(hit_message);
        } else {
            std::string missed_message =
                "Missed, mistake " + std::to_string(hangman.get_mistakes()) +
                " out of " + std::to_string(hangman.get_max_mistakes()) + ".\n";
            interface_output(missed_message);
        }
        std::string message = "The word: " + hangman.get_guessed_word() + "\n";
        interface_output(message);
    }
    std::string end_message;
    if (hangman.get_status() == game_status::win) {
        end_message = "You won!";
    } else if (hangman.get_status() == game_status::lose) {
        end_message = "You lost!";
    }
    interface_output(end_message);
    return 0;
}
