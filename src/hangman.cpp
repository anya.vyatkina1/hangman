#include "hangman.h"

Hangman::Hangman() {
    hidden_word = get_random_word();
    guessed_word = std::string(hidden_word.size(), '*');
    mistakes = 0;
    max_mistakes = 5;
}

Hangman::Hangman(std::string word, int max_mist) {
    hidden_word = word;
    guessed_word = std::string(hidden_word.size(), '*');
    max_mistakes = max_mist;
    mistakes = 0;
}

std::string Hangman::get_random_word() const {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 generator(seed);
    size_t word_ind = generator() % vocabulary.size();
    return vocabulary[word_ind];
}

bool Hangman::hit_or_missed(char guessed_letter) {
    bool hit = false;
    for (size_t i = 0; i < hidden_word.size(); i++) {
        if (hidden_word[i] == guessed_letter) {
            guessed_word[i] = guessed_letter;
            hit = true;
        }
    }
    if (!hit) mistakes++;
    if (mistakes == max_mistakes) {
        status = game_status::lose;
    }
    if (hidden_word == guessed_word) {
        status = game_status::win;
    }
    return hit;
}

std::string Hangman::get_guessed_word() const { return guessed_word; }

int Hangman::get_mistakes() const { return mistakes; }

int Hangman::get_max_mistakes() const { return max_mistakes; }

game_status Hangman::get_status() const { return status; }
