#include "../src/hangman.h"
#include <functional>
#include <cassert>
#include <iostream>

void test_get_status() {
    std::cout << "test_get_status" << std::endl;
    Hangman hangman_1("a", 2), hangman_2("a", 1);
    assert(hangman_1.get_status() == game_status::playing);
    assert(hangman_2.get_status() == game_status::playing);
    hangman_1.hit_or_missed('b');
    assert(hangman_1.get_status() == game_status::playing);
    hangman_1.hit_or_missed('c');
    assert(hangman_1.get_status() == game_status::lose);
    hangman_2.hit_or_missed('a');
    assert(hangman_2.get_status() == game_status::win);
    std::cout << "PASSED" << std::endl;
}

void test_hit_or_missed() {
    std::cout << "test_hit_or_missed" << std::endl;
    Hangman hangman("a", 2);
    assert(hangman.hit_or_missed('a'));
    assert(!hangman.hit_or_missed('b'));
    std::cout << "PASSED" << std::endl;
}

void test_get_mistakes() {
    std::cout << "test_get_mistakes" << std::endl;
    Hangman hangman;
    assert(hangman.get_mistakes() == 0);
    std::cout << "PASSED" << std::endl;
}

void  test_get_guessed_word() {
    std::cout << "test_get_guessed_word" << std::endl;
    Hangman hangman;
    std::string guessed = hangman.get_guessed_word();
    assert(guessed.find_first_not_of('*') == std::string::npos);
    std::cout << "PASSED" << std::endl;
}

void test_get_max_mistakes() {
    std::cout << "test_get_max_mistakes" << std::endl;
    Hangman hangman1;
    assert(hangman1.get_max_mistakes() == 5);
    Hangman hangman2("a", 3);
    assert(hangman2.get_max_mistakes() == 3);
    std::cout << "PASSED" << std::endl;
}

int main(int argc, char **argv) {
    std::vector<std::function<void()>> tests = {
            test_get_status,
            test_hit_or_missed,
            test_get_mistakes,
            test_get_guessed_word,
            test_get_max_mistakes,
    };
    for (const auto &test : tests) {
        test();
        std::cout << std::endl;
    }
}
